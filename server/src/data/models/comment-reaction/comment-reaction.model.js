import { DataTypes } from 'sequelize';

const init = orm => {
  const CommentReaction = orm.define(
    'commentReaction',
    {
      isLike: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: true
      },
      isDisLike: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    },
    {}
  );

  return CommentReaction;
};

export { init };
