import { Abstract } from '../abstract/abstract.repository';

class CommentReaction extends Abstract {
  constructor({ commentReactionModel, commentModel }) {
    super(commentReactionModel);
    this._commentModel = commentModel;
  }

  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: ['commentReaction.id', 'comment.id'],
      where: { commentId, postId },
      include: [
        {
          model: this._postModel,
          attributes: ['id', 'userId']
        }
      ]
    });
  }
}

export { CommentReaction };
