import * as React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Modal } from 'src/components/common/common';
import {
  image as imageService
} from 'src/services/services';
import AddPost from '../add-post/add-post';

import styles from './styles.module.scss';

const UpdatePost = ({ postId, onPostUpdate, removePost, closeUpdate }) => {
  const { posts } = useSelector(state => ({
    posts: state.posts.posts
  }));
  const postToUpdate = posts.find(post => post.id === postId);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <Modal open onClose={closeUpdate}>
      <Modal.Header className={styles.header}>
        <span>Update Post</span>
      </Modal.Header>
      <Modal.Content>
        <AddPost
          onPostAdd={onPostUpdate}
          uploadImage={uploadImage}
          removePost={removePost}
          postToUpdate={postToUpdate}
          closeUpdate={closeUpdate}
        />
      </Modal.Content>
    </Modal>
  );
};

UpdatePost.propTypes = {
  postId: PropTypes.string.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  removePost: PropTypes.func.isRequired,
  closeUpdate: PropTypes.func.isRequired
};

export default UpdatePost;
