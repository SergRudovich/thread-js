import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonType } from 'src/common/enums/enums';
import { Button, Form } from 'src/components/common/common';
import { Button as ButtonUI } from 'semantic-ui-react';

const UpdateComment = ({ postId, commentId, onCommentUpdate, deleteComment, onClose, commentBody }) => {
  const [body, setBody] = React.useState(commentBody);

  const handleAddComment = async () => {
    if (!body) {
      return;
    }
    await onCommentUpdate({ postId, body });
    await deleteComment(commentId);
    onClose(undefined);
    setBody('');
  };

  return (
    <Form reply onSubmit={handleAddComment}>
      <Form.TextArea
        value={body}
        placeholder=""
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type={ButtonType.SUBMIT} isPrimary>
        Update comment
      </Button>
      <ButtonUI onClick={() => onClose(undefined)}>
        Close
      </ButtonUI>
    </Form>
  );
};

UpdateComment.propTypes = {
  onCommentUpdate: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
  commentId: PropTypes.string.isRequired,
  commentBody: PropTypes.string.isRequired
};

export default UpdateComment;
