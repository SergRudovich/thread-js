import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { useSelector } from 'react-redux';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon, Card, Label } from 'src/components/common/common';
import { IconName } from 'src/common/enums/enums';
import { Button as ButtonUI } from 'semantic-ui-react';
import { commentType } from 'src/common/prop-types/prop-types';
import PropTypes from 'prop-types';
import UpdateComment from '../update-comment/update-comment';

import styles from './styles.module.scss';

const Comment = ({
  comment: { id, body, createdAt, user },
  onCommentLike,
  onCommentDislike,
  deleteComment,
  updateComment }) => {
  const { username, post } = useSelector(state => ({
    username: state.profile.user.username,
    post: state.posts.expandedPost
  }));
  const [updateCommentId, setUpdateCommentId] = React.useState(undefined);
  const likeCommentCount = 0;
  const dislikeCommentCount = 0;

  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        <CommentUI.Text>{body}</CommentUI.Text>
        <Card.Content extra>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handleCommentLike(id)}
          >
            <Icon name={IconName.THUMBS_UP} />
            {likeCommentCount}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handleCommentDislike(id)}
          >
            <Icon name={IconName.THUMBS_DOWN} />
            {dislikeCommentCount}
          </Label>
          {username === user.username
        && (
          <ButtonUI.Group size="small">
            <ButtonUI onClick={() => setUpdateCommentId(id)}>Update</ButtonUI>
            <ButtonUI.Or />
            <ButtonUI onClick={() => deleteComment(id)}>Delete</ButtonUI>
          </ButtonUI.Group>
        )}
          {updateCommentId && (
            <UpdateComment
              postId={post.id}
              commentId={id}
              onCommentUpdate={updateComment}
              deleteComment={deleteComment}
              onClose={setUpdateCommentId}
              commentBody={body}
            />
          )}
        </Card.Content>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  deleteComment: PropTypes.func,
  updateComment: PropTypes.func
};

Comment.defaultProps = {
  deleteComment: undefined,
  updateComment: undefined
};

export default Comment;
