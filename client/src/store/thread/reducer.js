import { createReducer } from '@reduxjs/toolkit';
import {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  deletePost,
  deleteComment
} from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  hasMorePosts: true
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setPosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addMorePosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addPost, (state, action) => {
    const { post } = action.payload;
    state.posts = [post, ...state.posts];
  });
  builder.addCase(deletePost, (state, action) => {
    const { post } = action.payload;
    state.posts = state.posts.filter(p => p.id !== post.id);
  });
  builder.addCase(setExpandedPost, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(deleteComment, (state, action) => {
    const { commentId } = action.payload;
    state.expandedPost.comments = state.expandedPost.comments.filter(p => p.id !== commentId);
  });
});

export { reducer };
